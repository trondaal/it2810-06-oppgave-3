var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
var flash = require('connect-flash');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var favicon = require('serve-favicon');
var path = require('path');
var dbConfig = require('./config/database');
require('./config/passport')(passport)

mongoose.connect(process.env.MONGODB_URI || dbConfig.url);

var app = express();

app.set('env', 'development');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: "secret",
    // name: "session",
    // rolling: true,
    // store: sessionStore, // connect-mongo session store
    // proxy: true,
    resave: false,
    saveUninitialized: false,
    // cookie: {
    //     secure: false,
    //     httpOnly: false
    // }
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

var routes = require('./routes/index');
var user = require('./routes/user');
var api = require('./routes/api');
var favorites = require('./routes/favorites');

app.use('/api', api);
app.use('/user/favorites', favorites);
app.use('/user', user);
app.use('/', routes);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.send(err.stack);
        // res.send('error', {
        //     message: err.message,
        //     error: err
        // });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send(err);
    // res.send('error', {
    //     message: err.message,
    //     error: {}
    // });
});

module.exports = app;