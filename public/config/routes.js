/**
 * Created by larsk on 29-Oct-16.
 */
import React from "react";
import {Router, Route, IndexRoute, browserHistory, useRouterHistory} from "react-router";
import {App} from "gameguru/App";
import {Login} from "gameguru/Login";
import {Signup} from "gameguru/Signup";
import {Profile} from "gameguru/Profile";
import {Search} from "gameguru/Search";
import Home from "gameguru/Home";
import NotImplemented from "gameguru/NotImplemented";
import GameDetail from "gameguru/GameDetail";
import Favorites from "gameguru/Favorites";
import Stats from 'gameguru/Stats'
// import createHistory from 'history/lib/createBrowserHistory';

// const history = useRouterHistory(createHistory)({
//     basename: '/'
// })

class Routes extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path='/' component={App}>
                    <IndexRoute component={Home}/>
                    <Route path='login' component={Login} />
                    <Route path='signup' component={Signup}/>
                    <Route path='favorites' component={Favorites}/>
                    <Route path='stats' component={Stats}/>
                    <Route path='profile' component={Profile}/>
                    <Route path='user/favorites' component={NotImplemented}/>
                    <Route path='user/:userId' component={NotImplemented}/>
                    <Route path='search' component={Search}/>
                    <Route path='games/:id' component={GameDetail}/>
                    {/*<Route path='discover' component={Discover}/>*/}
                </Route>
                {/*<Route path='*' component={NotImplemented}/>*/}
            </Router>
        )
    }
}

export default Routes;
