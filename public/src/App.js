import React from "react";
import NavBar from "gameguru/components/NavBar";
import Footer from "gameguru/components/Footer";
import * as auth from 'gameguru/auth';

export class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {user: null}
    }


    componentWillMount() {
        auth.verify().then(
            r => this.setState({authenticated: r}));
    }

    componentWillReceiveProps(newProps) {
        auth.verify().then(
            r => this.setState({authenticated: r}));
    }

    render() {
        return (
            <div id="app">
                <NavBar/>
                <div id="content">
                    {this.props.children}
                </div>
                <Footer />
            </div>);
    }
}

export default App;