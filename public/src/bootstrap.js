/**
 * Created by larsk on 12-Oct-16.
 */
import React from "react";
import ReactDOM from "react-dom";
import Routes from "gameguru/../config/routes";

let container = document.getElementById('container');
let component = ReactDOM.render(React.createElement(Routes, {id: 'react-root'}), container);
