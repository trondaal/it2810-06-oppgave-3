/**
 * Created by larsk on 14-Oct-16.
 */
import React from "react";

export class Profile extends React.Component {
    render() {
        return (
            <div>
                <div className="page-header text-center">
                    <h1><span className="fa fa-anchor"></span> Profile Page</h1>
                    <a href="/logout" className="btn btn-default btn-sm">Logout</a>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <div className="well">
                            <h3><span className="fa fa-user"></span> Local</h3>

                            <p>
                                <strong>id</strong>:
                                {this.props.user.id}><br/>
                                <strong>email</strong>:
                                {this.props.user.email}
                                <br/>
                                <strong>password</strong>:
                                {this.props.user.password}
                            </p>

                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default Profile;
