import React from 'react';
export default class Discover extends React.Component {
    render() {
        return (
            <div>
                <div id="popular">
                    <h2>Popular now</h2>
                    <hr className="line" id="shortline"/>
                    <br/>
                    <div className="game-box" id="p1"></div>
                </div>

                <div id="reviews">
                    <h2>New Reviews</h2>
                    <hr className="line" id="shortline"/>
                    <br/>
                    <div className="game-box" id="r1"></div>
                </div>
            </div>
        )
    }
};
