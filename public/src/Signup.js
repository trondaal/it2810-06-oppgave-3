/**
 * Created by larsk on 14-Oct-16.
 */
import React from "react";
import {Link, IndexLink} from "react-router";
import * as auth from "gameguru/auth";

export class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.submit = this.submit.bind(this);
    }

    handleEmailChange(e) {
        this.setState({email: e.target.value});
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }

    submit(e) {
        e.preventDefault();
        let r = auth.signup(this.state.email, this.state.password);
        r.then(r => {
                if (r) this.props.router.replace('/')
                else this.setState({message: "An account already exists with that email."})
            }
        )
    }

    render() {
        return (<div className="col-sm-6 col-sm-offset-3">

            <h1><span className="fa fa-sign-in"></span> Signup</h1>
            {this.state && this.state.message && <div className="alert alert-danger">{this.state.message}</div>}

            <form action="" method="post" onSubmit={this.submit}>
                <div className="form-group">
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" onChange={this.handleEmailChange}/>
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name="password"
                           onChange={this.handlePasswordChange}/>
                </div>

                <button className="btn btn-warning btn-lg">Signup</button>
            </form>

            <hr/>

            <p>Already have an account? <Link to='/login'>Login</Link></p>
            <p>Or go <IndexLink to="/">Home</IndexLink>.</p>

        </div>);
    }
}

export default Signup;