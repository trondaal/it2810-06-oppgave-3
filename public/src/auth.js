/**
 * Created by larsk on 29-Oct-16.
 */
export {login, logout, signup, isAuthenticated, verify}

function login(email, password) {
    return fetch('/login_ajax', {
        method: 'POST',
        body: JSON.stringify({email: email, password: password}),
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(r => r.ok ? r.json() : null)
        .then(json => {
            if (json) {
                sessionStorage.setItem('userId', json._id)
                return true;
            }
            else {
                console.log("Could not sign in.");
                return false;
            }
        })
        .catch(e => {
            console.log(e);
            return false;
        })
}

function signup(email, password) {
    return fetch('/signup_ajax', {
        method: 'POST',
        body: JSON.stringify({email: email, password: password}),
        credentials: 'same-origin',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    })
        .then(r => r.ok ? r.json() : false)
        .then(json => {
            if (json) {
                sessionStorage.setItem('userId', json._id)
                return true;
            }
            else {
                console.log("Could not sign in.");
                return false;
            }
        })
        .catch(e => {
            console.log(e);
            return false;
        })
}


function logout() {
    return fetch('/user/logout_ajax', {
        credentials: 'same-origin',
    })
        .then(r => {
            if (r.ok) {
                sessionStorage.removeItem('userId');
                return true;
            } else {
                return false;
            }
        })
}

function isAuthenticated() {
    return sessionStorage.getItem('userId') ? true : false;
}

function verify() {
        return fetch('/user/verify', {
            credentials: 'same-origin',
        })
            .then(r => r.ok ? r.json() : false)
            .then(json => {
                if (json) {
                    sessionStorage.setItem('userId', json._id)
                    return true;
                } else {
                    sessionStorage.removeItem('userId');
                    return false;
                }
            })
}



