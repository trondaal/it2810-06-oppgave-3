import React from "react";
import GameRow from "gameguru/components/GameRow";
import DisplayOriginalLogo from "gameguru/components/DisplayOriginalLogo";
import DisplayStartInfo from "gameguru/components/DisplayStartInfo";
import "whatwg-fetch";

export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {}
    }

    fetchPopular() {
        fetch('/api/games/?fields=name,rating,cover&limit=5&order=popularity:desc')
            .then(
                r => r.json()
            ).then(json =>
            this.setState({popular: json})
        )
    }

    fetchRecent() {
        fetch('/api/games/?fields=name,release_dates,cover&limit=5'
        + '&order=release_dates.date:desc&filter[release_dates.date][lt]='
        + currentString + '&filter[release_dates.date][gte]=' + pastString)
            .then(
                r => r.json()
            ).then(json =>
            this.setState({recent: json})
        )
    }

    fetchUpcoming() {
        fetch('/api/games/?fields=name,release_dates,cover&limit=5'
        + '&order=release_dates.date:desc&filter[release_dates.date][gt]='
        + currentString + '&filter[release_dates.date][lt]=' + futureString)
            .then(
                r => r.json()
            ).then(json =>
            this.setState({upcoming: json})
        )
    }

    componentWillMount() {
        this.fetchPopular();
        this.fetchRecent();
        this.fetchUpcoming();
    }

    render() {
        return (
            <div id="home">
                    <DisplayOriginalLogo />
                    <DisplayStartInfo />
                <GameRow header='Popular' games={this.state.popular}/>
                <GameRow header='Recent' games={this.state.recent}/>
                <GameRow header='Upcoming' games={this.state.upcoming}/>
            </div>
        )
    }
};

var currentDate = new Date();
var pastDate = new Date();
var futureDate = new Date();
Date.prototype.yyyymmdd = function() {
  var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
  var dd = this.getDate().toString();

  return [this.getFullYear() + '-', mm.length===2 ? '' : '0', mm + '-', dd.length===2 ? '' : '0',dd].join(''); // padding
};

pastDate.setDate(pastDate.getDate() - 7)
futureDate.setDate(futureDate.getDate() + 14);

var pastString = pastDate.yyyymmdd();
var currentString = currentDate.yyyymmdd();
var futureString = futureDate.yyyymmdd();
