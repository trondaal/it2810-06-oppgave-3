import React from "react";


const SELECTED_STYLE = {
    color: 'red'
};

export default class PageSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {selected: 0}
    }

    setSelected = (page) => {
        this.setState({selected: page});
        this.props.onChange(page);
    };

    getStyle = (page) => {
        return this.state.selected == page ? SELECTED_STYLE : {};
    };

    componentWillReceiveProps = (nextProps) =>{
        const nPages = Math.ceil(nextProps.n / nextProps.nPerPage);
        if(this.state.selected > nPages){
            const newSelected = nPages - 1;
            this.setSelected(newSelected);
        }
    };

    render() {
        let pages = [];
        for (let i = 0; i < Math.ceil(this.props.n / this.props.nPerPage); i++) {
            pages.push(i);
        }
        return (
            <div>
                { pages.map((i) => <button style={this.getStyle(i)} key={i} onClick={() => this.setSelected(i)}>{i + 1}</button>)}
            </div>
        )
    }
}

