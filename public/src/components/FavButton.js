// star-button to add a game to favourites
import React from "react";
import {ButtonGroup, Button, Glyphicon} from "react-bootstrap";
import Authenticated from 'gameguru/components/Authenticated';
import * as auth from 'gameguru/auth';

export class FavButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentWillMount() {
        if (auth.isAuthenticated())
            this.fetchFavorites();
    }

    fetchFavorites() {
        fetch('/user/favorites', {
            credentials: 'same-origin'
        }).then(
            r => r.json()).then(
            json => this.setState({active: json.includes(this.props.id)}));
    }
// a function to add a game to favourites:
    addFavorite() {
        // fetch('/user/favorites/add/' + this.props.id, {
        fetch('/user/favorites', {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: this.props.id,
            })
        })
            .then(
                this.setState({active: true})
            );
    }
//a function to remove a game from favourites:
    removeFavorite() {
        // fetch('/user/favorites/remove/' + this.props.id, {
        fetch('/user/favorites/' + this.props.id, {
            method: 'DELETE',
            credentials: 'same-origin'
        })
            .then(
                this.setState({active: false})
            );
    }

    isFavourite() {
        return this.state.active ? "warning" : "default"
    }
//clicking on the button adds a game to favourites, clicking one more time removes it:
    clickHandler() {
        if (this.state.active)
            this.removeFavorite();
        else
            this.addFavorite();
    }
//the button uses react bootstrap for styling:
    render() {
        return (
            <Authenticated>
                <ButtonGroup>
                    <Button onClick={this.clickHandler.bind(this)} active={this.state.active}
                            bsStyle={this.isFavourite()}>
                        <Glyphicon glyph="star"/>
                    </Button>
                </ButtonGroup>
            </Authenticated>
        );
    }
}

export default FavButton;