//component that dispalys dropdown menu used in filtering and sorting
import React from "react";
import {DropdownButton, MenuItem} from "react-bootstrap";

export default class Dropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = { index: -1}
    }

    onChange = (i) => {
        this.setState({index: i});
        this.props.onChange(this.props.name, i);
    };

    getTitle = () => {
        if(this.state.index == -1 ) return this.props.defaultTitle;
        return this.props.elements[this.state.index];
    };

    render() {
        return (
            <DropdownButton id="dropdownbutton" onSelect={this.onChange} title={this.getTitle()}>
                <MenuItem key={-1} eventKey={-1}>{this.props.defaultTitle}</MenuItem>
                {Object.keys(this.props.elements).map((key) => <MenuItem key={key} eventKey={key}>{this.props.elements[key]}</MenuItem>)}
            </DropdownButton>
        )
    }
}
