//footer that displays names of the group members
import React from 'react';

export default class Footer extends React.Component{
	render(){
		return(
		<footer>
			<p className="text-muted"> Created by <br/>Lars Kristian Dahl, Jonas J. Husebø, Agata Jedryszek, Jonas Laskemoen, Espen Sivertsen, Camilla Tran. </p>
		</footer>
			);
	}
}
