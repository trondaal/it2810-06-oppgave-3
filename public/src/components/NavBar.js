//component consisting of navigation bar for the webside. 
import React from "react";
import {Link, IndexLink, withRouter} from "react-router";
import DisplayNavLogo from "gameguru/components/DisplayNavLogo";
import Authenticated from "gameguru/components/Authenticated";
import {logout, verify} from "gameguru/auth";

const navBarElements = [
    {
        title: 'Search',
        url: '/search'
    },
    {
        title: 'Stats',
        url: '/stats'

    }
];

class NavBar extends React.Component {

    constructor(props) {
        super(props);
        this.logoutHandler = this.logoutHandler.bind(this);
    }


    logoutHandler(e) {
        e.preventDefault();
        logout('/')
            .then(r => r && this.props.router.replace('/'))
    }

    render() {
        return (
            <div>
                <nav id="menu">
                    <ul>
                        <li> 
                            <IndexLink to="/"><DisplayNavLogo /></IndexLink>
                        </li>
                        
                        <li key="0">
                            <IndexLink to="/">Home</IndexLink>
                        </li>
                        {navBarElements.map((e, i) =>
                            <li key={i}>
                                <Link to={e.url}>{e.title}</Link>
                            </li>
                        )}
                        <li key="favorites">
                            <Authenticated>
                                <Link to="/favorites">My Favorites</Link>
                            </Authenticated>
                        </li>
                        <li key="login">
                            <Authenticated value={false}>
                                <Link to="/login">Login</Link>
                            </Authenticated>
                        </li>
                        <li key="signup">
                            <Authenticated value={false}>
                                <Link to="/signup">Signup</Link>
                            </Authenticated>
                        </li>

                        <li key="logout">
                            <Authenticated>
                                <a onClick={this.logoutHandler}>Logout</a>
                            </Authenticated>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default withRouter(NavBar);
