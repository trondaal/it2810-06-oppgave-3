/**
 * Created by larsk on 30-Oct-16.
 */
import React from "react";

function IgdbImage(props) {
    if (!props.source) {
      return <img height="200px" width="150px" src='img/notavailable.png'/>
    } else {
      return <img height={props.height} width={props.width} src={url + props.size + '_2x/' + props.source.cloudinary_id + '.jpg'}/>
    }
}

const url = 'https://res.cloudinary.com/igdb/image/upload/t_';

export default IgdbImage;
