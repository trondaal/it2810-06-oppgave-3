//component that displays full version of the logo; used on home page below navbar
import React from "react";

export default function DisplayOriginalLogo(props) {
    return <div id="originalLogo"><img src="../../img/originalLogo.png"/></div>
}

