/**
 * Created by larsk on 30-Oct-16.
 */
 //function that converts date to String; used on pages for specific games inside ReleaseDateCollection
import React from 'react';

function TimestampDate(props) {
    return <span className="date"> on {new Date(props.value).toDateString()}</span>
}

export default TimestampDate;