//component that displays a collection of videos (see Video.js)
import React from 'react';
import Video from 'gameguru/components/Video';

class VideoCollection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {collection: props.collection}
    }
    render() {
        return <div className="video_collection">
            {this.props.source && this.props.source.map((obj, i) => {
                return <div key={i} className="screenshot">
                <Video video_id={obj.video_id}/>
                </div>
            })}
        </div>
    }
}
export default VideoCollection;
