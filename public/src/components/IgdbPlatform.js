/**
 * Created by larsk on 29-Oct-16.
 */
 //component showing platform game was released for; used on pages for specific games inside ReleaseDateCollection
import React from "react";

class IgdbPlatform extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: props.value}
    }

    componentWillMount() {
        this.fetch();
    }


    fetch() {
        // TODO: Check for null in json[0]
        fetch('/api/platforms/' + this.state.value + '?fields=name')
            .then(r => r.json())
            .then(json => this.setState({
                    value: json[0].name
                })
            );
    }

    render() {
        return <span className="platform"> for {this.state.value} </span>;
    }
}

export default IgdbPlatform;