/**
 * Created by larsk on 29-Oct-16.
 */
 //comopent authenticating user
import React from 'react';
import {isAuthenticated} from 'gameguru/auth';

class Authenticated extends React.Component {

    static defaultProps = {
        value: true
    };

    render() {
        let authenticated = isAuthenticated();
        return ((!authenticated && !this.props.value) || authenticated == this.props.value) ? this.props.children : null;

    }

}

export default Authenticated;