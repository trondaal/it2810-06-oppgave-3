//component that displays search results list
import React from "react";
import {Link} from "react-router";
import {ListGroup, ListGroupItem} from "react-bootstrap";
export default class GameList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {games: props.games};
    }


    componentWillReceiveProps(nextProps) {
        this.setState({games: nextProps.games});
    }

//React Boostrap is used to style the list, and every result links to a page for the specific game
    render() {
        return (
            <ListGroup>
            {this.state.games.map(
                    (game, i) =>
                        <ListGroupItem key={game.id}><Link to={'/games/' + game.id}>{game.name} </Link></ListGroupItem>
                )}
            </ListGroup>
            
        )
    }
};
