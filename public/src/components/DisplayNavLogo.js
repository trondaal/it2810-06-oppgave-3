//function that displays logo in the navbar to the left
import React from "react";

export default function DisplayNavLogo() {

        return (
                <img id="navLogo" src="../../img/navlogo.png"/>
        );
}