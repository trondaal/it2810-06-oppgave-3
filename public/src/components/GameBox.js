/**
 * Created by larsk on 15-Oct-16.
 */
//component for a single game; it serves as a button that displays game's cover. It links to a page for the game that
//consists of all information about the game gathered from Igdb. 
import React from "react";
import {Link} from "react-router";
import IgdbImage from 'gameguru/components/IgdbImage';


export default class GameBox extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <li>
                <Link to={'/games/' + this.props.game.id}>
                    <div className="game-box">
                        <IgdbImage width="150px" height="200px" size="cover_med" source={this.props.game.cover}/>
                        {/*<div className="title">{this.props.game.name}</div>*/}
                        {/* <div className="rating">{this.props.game.rating}</div>*/}
                        {/*{this.props.game.release_dates.map(rdate =>*/}
                        {/*<div key={rdate.platform}className="release-date">{new Date(rdate.date).toDateString()}</div>*/}
                        {/*)}*/}
                    </div>
                </Link>
                <h4 className="game-title">{this.props.game.name}</h4>
            </li>
        );

    }
}
