/**
 * Created by larsk on 29-Oct-16.
 */
 //component that shows details of game's release - category, platform, date and region. 
import React from "react";
import IgdbCategory from 'gameguru/components/IgdbCategory';
import IgdbPlatform from 'gameguru/components/IgdbPlatform';
import IgdbRegion from 'gameguru/components/IgdbRegion';
import TimestampDate from 'gameguru/components/TimestampDate';

class ReleaseDateCollection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {collection: props.collection}
    }

    render() {
        return <div className="release_dates">
            {this.state.collection.map((obj, i) => {
                return <div key={i} className="release_date">
                    <IgdbCategory value={obj.category}/>
                    <IgdbPlatform value={obj.platform}/>
                    <TimestampDate value={obj.date}/>
                    <IgdbRegion value={obj.region}/>
                </div>
            })}
        </div>
    }
}

export default ReleaseDateCollection;