/**
 * Created by larsk on 29-Oct-16.
 */
import React from "react";

function IgdbEsrb(props) {
    return (<span className="esrb-rating">{map[props.value]}</span>);
}

const map = {
    1: 'RP',
    2: 'EC',
    3: 'E',
    4: 'E10+',
    5: 'T',
    6: 'M',
    7: 'AO'
}

export default IgdbEsrb;