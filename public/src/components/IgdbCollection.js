/**
 * Created by larsk on 29-Oct-16.
 */
import React from "react";

class IgdbCollection extends React.Component {

    constructor(props) {
        super(props);
        var arr = props.collection ? [].concat(props.collection) : [];
        this.state = {collection: arr}
    }

    componentWillMount() {
        this.fetch();
    }

    fetch() {
        if (this.state.collection.length == 0) return;
        fetch('/api/' + this.props.api + '/' + this.state.collection.join(',') + '?fields=name')
            .then(r => r.json())
            .then(json => this.setState({
                    collection: json
                })
            );
    }

    render() {
        return (<span>
            {this.state.collection.map(
                (value, i) => i > 0 ? ', ' + value.name : value.name
            )}
        </span>);
    }
}

export default IgdbCollection;