//component that displays youtube video relevant to a specific game
import React from 'react';
import YouTube from 'react-youtube';

class Video extends React.Component {

  constructor(props) {
      super(props);
      this.state = {games: this.props.games};
  }

  render() {
    const opts = {
      height: '390',
      width: '640',
      playerVars: { // https://developers.google.com/youtube/player_parameters
        autoplay: 1
      }
    };

    return (
        <YouTube
          videoId={this.props.video_id}
          opts={opts}
          onReady={this._onReady}
        />
    );
  }

  _onReady(event) {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  }
}

export default Video;
