/**
 * Created by larsk on 29-Oct-16.
 */
 //component showing in which regions game was released; used on pages for specific games inside ReleaseDateCollection
import React from "react";

function IgdbRegion(props) {
    return <span className="region">, {map[props.value]} </span>
}

const map = {
    1: 'Europe',
    2: 'North America',
    3: 'Australia',
    4: 'New Zealand',
    5: 'Japan',
    6: 'China',
    7: 'Asia',
    8: 'Worldwide'
}

export default IgdbRegion;
