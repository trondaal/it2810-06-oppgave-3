//component to display a row of games (GameBoxes); used to show a few games htat are popular now, a few upcoming games etc.
import React from "react";
import GameBox from "gameguru/components/GameBox";

class GameRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {games: this.props.games};
    }

    componentWillReceiveProps(nextProps) {
        this.setState({games: nextProps.games});
    }

    render() {
        if (this.state.games == null) return (<div></div>);
        return (<div className="game-row">
                <div id={this.props.header}>
                    <h2>{this.props.header}</h2>
                    <hr className="line shortline"/>
                    <br/>
                    <ul>
                    {this.state.games.map((o, i) => {
                        return <GameBox game={o} key={i}/>
                    })}
                    </ul>
                </div>
            </div>
        );
    }
}

export default GameRow;


