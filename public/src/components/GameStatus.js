/**
 * Created by larsk on 29-Oct-16.
 */
 //function showing game's status; used on pages for specific games
import React from "react";

function GameStatus(props) {
    return (<span className="status"> {map[props.value]}</span>);
}

const map = {
    2: 'Alpha',
    3: 'Beta',
    4: 'Early Access',
    5: 'Offline',
    6: 'Cancelled',
};

export default GameStatus;