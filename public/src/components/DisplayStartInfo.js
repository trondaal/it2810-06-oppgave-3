//function that displays the start info
import React from "react";

export default function DisplayStartInfo() {

        return (
                <div className="infobox">
                	Welcome to Game Guru! This site shows an overview of different
                	games. To favor a game and save it, please sign up.
                </div>
        );
}