//search bar with a magnifying glass at the end of the input box; uses react Bootstrap for styling
import React from "react";
import {FormGroup, FormControl, InputGroup, Glyphicon} from "react-bootstrap";

export default class FavoriteSearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ""};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
        this.props.onChange(event);
    }

    render() {
        return (
            <form>
                <FormGroup>
                    {/*<FormControl onChange={this.handleChange} value={this.state.value} type="text"/>*/}
                    <FormControl onChange={this.handleChange} value={this.state.value} type="text" placeholder="Search in your favorites..."/>
                    <InputGroup.Addon>
                        <Glyphicon glyph="search"/>
                    </InputGroup.Addon>
                </FormGroup>
            </form>);
    }
}
