/**
 * Created by larsk on 29-Oct-16.
 */
 //component showing category of the game release; used on pages for specific games inside ReleaseDateCollection
import React from "react";

function IgdbCategory(props) {
    return (<span className="category">{map[props.value]}</span>);
}

const map = {
    0: 'Full Game',
    1: 'DLC/Addon',
    2: 'Expansion',
    3: 'Bundle',
    4: 'Standalone Expansion'
}

export default IgdbCategory;