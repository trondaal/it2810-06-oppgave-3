/**
 * Created by larsk on 15-Oct-16.
 */
import React from "react";
import IgdbCategory from "gameguru/components/IgdbCategory";
import {FavButton} from "gameguru/components/FavButton";
import ReleaseDateCollection from "gameguru/components/ReleaseDateCollection";
import IgdbCollection from "gameguru/components/IgdbCollection";
import TimestampDate from "gameguru/components/TimestampDate";
import IgdbImage from 'gameguru/components/IgdbImage';
import Gallery from 'gameguru/components/Gallery';
import VideoCollection from 'gameguru/components/VideoCollection';

function makeCloudinarySrc(size, id) {
  return 'https://res.cloudinary.com/igdb/image/upload/t_'+ size + '_2x/'+ id + '.jpg'
}

export default class GameDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {id: parseInt(props.params.id)}
    }

    componentWillMount() {
        this.fetchGame();
    }

    fetchGame() {
        // TODO: check for null in json[0]
        fetch('/api/games/' + this.state.id + '/?fields=*')
            .then(
                r => r.json()
            ).then(json =>
            this.setState({game: json[0]})
        )
    }

    render() {
        if (this.state == null || this.state.game == null) return null;
        return (
            <div className="game-details">

                <div id="gamecontainer">
                    <div className="cover-image">
                        <IgdbImage size="logo_med" source={this.state.game.cover}/>
                    </div>

                    <h2 className="title">{this.state.game.name}</h2>

                    <div className="favorite">
                        <FavButton id={this.state.game.id}/>
                    </div>

                    <hr className="line" id="shortline"/>
                </div>

                <div id="gamecontent">
                    <h2>Game Details</h2>
                    <table>
                        <tbody>
                        <tr>
                            <th>Title:</th>
                            <td>{this.state.game.name}</td>
                        </tr>
                        <tr>
                            <th>First release date:</th>
                            <td><TimestampDate value={this.state.game.first_release_date}/></td>
                        </tr>
                        <tr>
                            <th>Release dates:</th>
                            <td><ReleaseDateCollection collection={this.state.game.release_dates}/></td>
                        </tr>
                        <tr>
                            <th>Genres:</th>
                            <td><IgdbCollection api="genres" collection={this.state.game.genres}/></td>
                        </tr>
                        <tr>
                            <th>Themes:</th>
                            <td><IgdbCollection api="themes" collection={this.state.game.themes}/></td>
                        </tr>
                        <tr>
                            <th>Category:</th>
                            <td><IgdbCategory value={this.state.game.category}/></td>
                        </tr>
                        <tr>
                            <th>Publishers:</th>
                            <td><IgdbCollection api="companies" collection={this.state.game.publishers}/></td>
                        </tr>
                        <tr>
                            <th>Developers:</th>
                            <td><IgdbCollection api="companies" collection={this.state.game.developers}/></td>
                        </tr>
                        <tr>
                            <th>Game engines:</th>
                            <td><IgdbCollection api="game_engines" collection={this.state.game.game_engines}/></td>
                        </tr>
                        <tr>
                            <th>Game modes:</th>
                            <td><IgdbCollection api="game_modes" collection={this.state.game.game_modes}/></td>
                        </tr>
                        <tr>
                            <th>Keywords:</th>
                            <td><IgdbCollection api="keywords" collection={this.state.game.keywords}/></td>
                        </tr>
                        <tr>
                            <th>Collection:</th>
                            <td><IgdbCollection api="collections" collection={this.state.game.collection}/></td>
                        </tr>
                        <tr>
                            <th>Franchise:</th>
                            <td><IgdbCollection api="franchises" collection={this.state.game.franchise}/></td>
                        </tr>
                        <tr>
                            <th>Rating:</th>
                            <td>{this.state.game.rating} ({this.state.game.rating_count} votes)</td>
                        </tr>
                        <tr>
                            <th>Aggregated rating:</th>
                            <td>{this.state.game.aggregated_rating}</td>
                        </tr>
                        <tr>
                            <th>Popularity:</th>
                            <td>{this.state.game.popularity}</td>
                        </tr>
                        </tbody>
                    </table>

                    <hr className="line" id="shortline"/>
                </div>
                <div id="gamedescription">
                    <h2>Overview</h2>
                    <p className="summary">{this.state.game.summary}</p>
                </div>
                <div id="screenshots">
                  <h2>Screenshots</h2>
                  <Gallery images={this.state.game.screenshots.map(({cloudinary_id, height, width}) => ({
                    src: makeCloudinarySrc("screenshot_big", cloudinary_id),
                    thumbnail: makeCloudinarySrc("thumb", cloudinary_id)
                  }))} showThumbnails />
                </div>
                <div id="videos">
                  <h2>Videos</h2>
                  <VideoCollection source={this.state.game.videos}/>
                </div>
            </div>);

    }
}
