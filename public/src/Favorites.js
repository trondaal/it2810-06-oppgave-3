import React from "react";
import GameRow from "gameguru/components/GameRow";
import FavoriteSearchBar from 'gameguru/components/FavoriteSearchBar';
import "whatwg-fetch";

export default class Favorites extends React.Component {
    constructor(props) {
        super(props);
        this.onSearch = this.onSearch.bind(this);
        this.state = {};
    }

    onSearch(e) {
        if (e.target.value != "")
        this.setState({filtered: this.state.favorites.filter(t=> t.name.toLowerCase().indexOf(e.target.value.toLowerCase()) != -1)})
        else this.setState({filtered: this.state.favorites})
    }

    fetchFavorites() {
        fetch('/user/favorites/extended?fields=name,cover&limit=50', {
            credentials: 'same-origin',
        })
            .then(r => r.json())
            .then(json => {
                this.setState({favorites: json, filtered: json})
            })
    }

    componentWillMount() {
        this.fetchFavorites();
    }

    render() {
        return (
            <div >
                <FavoriteSearchBar onChange={this.onSearch}/>
                <GameRow header='Favorites' games={this.state.filtered}/>
            </div>
        )
    }
};
