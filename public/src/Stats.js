import React from "react";
import {HorizontalBar, Pie} from "react-chartjs-2";
export default class Stats extends React.Component {

    componentWillMount() {
        this.fetchGenres();
        this.fetchThemes();
        this.fetchGameModes();
    }

    shouldComponentUpdate(newProps, newState) {
        return (newState && newState.genres && newState.modes && newState.themes) ? true : false;
    }

    fetchGenres() {
        fetch('/api/genres/?fields=name,games&limit=50')
            .then(
                r => r.json()
            ).then(json =>
            this.setState({genres: json})
        )
    }

    fetchGameModes() {
        fetch('/api/game_modes/?fields=name,games&limit=50')
            .then(
                r => r.json()
            ).then(json =>
            this.setState({modes: json})
        )
    }

    fetchThemes() {
        fetch('/api/themes/?fields=name,games&limit=50')
            .then(
                r => r.json()
            ).then(json =>
            this.setState({themes: json})
        )
    }

    render() {
        if (!this.state) return null;
        let genres = {
            labels: this.state.genres.map(v => v.name),
            datasets: [
                {
                    label: "Games by Genre",
                    data: this.state.genres.map(v => v.games.length)
                }
            ]
        }
        let modes = {
            labels: this.state.modes.map(v => v.name),
            datasets: [
                {
                    label: "Games by Game Mode",
                    data: this.state.modes.map(v => v.games.length)
                }
            ]
        }
        let themes = {
            labels: this.state.themes.map(v => {
                if (v.id == 41) return '4X'; // Shorten the name
                else return v.name
            }),
            datasets: [
                {
                    label: "Games by Theme",
                    data: this.state.themes.map(v => v.games.length)
                }
            ]
        }
        return (
            <div id="stats">
                <HorizontalBar data={genres} width={500} height={600} />
                <HorizontalBar data={themes} width={500} height={600} />
                {/*<Pie data={modes} options={{*/}
                    {/*maintainAspectRatio: true*/}
                {/*}}/>*/}
            </div>
        )
    }
};
