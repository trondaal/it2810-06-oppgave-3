import React from "react";
import GameList from "gameguru/components/GameList";
import SearchBar from "gameguru/components/SearchBar";
import Dropdown from "gameguru/components/Dropdown";
import PageSelect from "gameguru/components/PageSelect";
import "whatwg-fetch";


const ORDERING_LIST = ['Alphabetical ordering', 'Release date'];
const ORDERING_FIELDS = ['name', 'first_release_date', 'relevance'];
const N_PER_PAGE = 10;

// TODO: Should load genres from the API
const GENRES = {
    2: "Point-and-click",
    4: "Fighting",
    5: "Shooter",
    7: "Music",
    8: "Platform",
    9: "Puzzle",
    10: "Racing",
    11: "Real Time Strategy (RTS)",
    12: "Role-playing (RPG)",
    13: "Simulator",
    14: "Sport",
    15: "Strategy",
    16: "Turn-based strategy (TBS)"
};
const CATEGORIES = {
    0: 'Main Game',
    1: 'DLC / Addon',
    2: 'Expansion',
    3: 'Bundle',
    4: 'Standalone Expansion'
};

export class Search extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            games: [],
            tempGames: [],
            value: "Search...",
            sortId: -1,
            genreId: -1,
            categoryId: -1,
            page: 0,
            nGames: 0
        }
        this.onSearch = this.onSearch.bind(this);
        this.setPage = this.setPage.bind(this);
        this.onChange = this.onChange.bind(this);
    };

    onSearch = (e) => {
        fetch('/api/games/?limit=50&fields=*&search=' + e.target.value)
            .then(r => r.json())
            .then(games => {
                console.log(games);
                this.setState({games, tempGames: games}, this.sortAndFilter)
            })
            .catch(err => console.log({err}))
    };

    onChange(name, i) {
        let state = {};
        state[name + 'Id'] = i;
        this.setState(state, this.sortAndFilter);
    };

    setPage(page) {
        this.setState({page}, this.sortAndFilter)
    }

    sortAndFilter() {
        let games = this.state.games;
        if (this.state.genreId != -1)
            games = games.filter(game => game.genres && game.genres.indexOf(parseInt(this.state.genreId)) > -1);
        if (this.state.categoryId != -1)
            games = games.filter(game => game.category == this.state.categoryId);
        if (this.state.sortId != -1)
            games.sort((a, b) => a[ORDERING_FIELDS[this.state.sortId]] > b[ORDERING_FIELDS[this.state.sortId]] ? 1 : -1);
        let nGames = games.length;
        games = games.slice(N_PER_PAGE * this.state.page, N_PER_PAGE * (this.state.page + 1));
        this.setState({tempGames: games, nGames})
    };

    render() {
        return (
            <div>
                <Dropdown defaultTitle="-- Sort by" name="sort" elements={ORDERING_LIST} onChange={this.onChange}/>
                <Dropdown defaultTitle="-- Genre" name="genre" elements={GENRES} onChange={this.onChange}/>
                <Dropdown defaultTitle="-- Category" name="category" elements={CATEGORIES} onChange={this.onChange}/>
                <SearchBar onChange={this.onSearch}/>
                <GameList games={this.state.tempGames}/>
                <PageSelect onChange={this.setPage} nPerPage={N_PER_PAGE} n={this.state.nGames}/>
            </div>
        )
    }
}

export default Search;