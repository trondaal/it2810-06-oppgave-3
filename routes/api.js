var express = require('express');
var igdb = require('../igdb-wrapper');
var router = express.Router();

var User = require('../models/user');
igdb.set_key("PsXs3W1kEwmshcWJ6GvKvOzcYR18p15jSKXjsnsz1eU17oFFZB");


var userId = '57fa8e0ab9d9601c583066fb';

// TODO: Use session userID
router.get('/favorites', function (req, res) {
    var userId = req.user.id;
    User.findById(userId,
        (err, user) => {
            let q = req.url.replace('favorites', 'games/' + user.local.favorites.join(','));
            console.log(q);
            igdb.get(q, json => res.send(json.body));
        });
});

/* GET games listing. */
router.get('*', function(req, res, next) {
    igdb.get(req.url, function (result) {
        res.json(result.body);
    });
});
module.exports = router;
