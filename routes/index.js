var express = require('express');
var passport = require('passport');

var router = express.Router();

router.post('/login', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) return next(err);
        if (!user) return res.status(403).send(info);
        req.logIn(user, err => {
            if (err) return next(err);
        });
        res.cookie('authenticated', 'true', {maxAge: 900000});
        // return res.send(user);
        return res.redirect('/');
    })(req, res, next);
});

router.post('/login_ajax', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) return next(err);
        if (!user) return res.status(403).send(info);
        req.logIn(user, err => {
            if (err) return next(err);
        });
        return res.send(user);
    })(req, res, next);
});

router.post('/signup', function (req, res, next) {
    passport.authenticate('local-signup', function (err, user, info) {
        if (err) return next(err);
        if (!user) return res.status(403).send(info);
        req.logIn(user, err => {
            if (err) return next(err);
        });
        res.cookie('authenticated', 'true', {maxAge: 900000});
        // return res.send(user);
        return res.redirect('/');
    })(req, res, next);
});

router.post('/signup_ajax', function (req, res, next) {
    passport.authenticate('local-signup', function (err, user, info) {
        if (err) return next(err);
        if (!user) return res.status(403).send(info);
        req.logIn(user, err => {
            if (err) return next(err);
        });
        return res.send(user);
    })(req, res, next);
});

// redirect everything else to the react router
router.get('*', function (req, res) {
    res.sendFile('index.html', {root: "public"});
});

module.exports = router;
