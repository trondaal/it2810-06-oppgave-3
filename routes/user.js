var express = require('express');
var passport = require('passport');
var igdb = require('../igdb-wrapper');

var router = express.Router();

// =====================================
// PROFILE SECTION =====================
// =====================================
// we will want this protected so you have to be logged in to visit
// we will use route middleware to verify this (the isLoggedIn function)
router.get('/profile', isLoggedIn, function (req, res) {
    res.render('profile.ejs', {
        user: req.user // get the user out of session and pass to template
    });
});

// =====================================
// LOGOUT ==============================
// =====================================
router.get('/logout', function (req, res) {
    req.logout();
    res.clearCookie('authenticated');
    res.redirect('/');
});

router.get('/logout_ajax', function (req, res) {
    req.logout();
    res.end();
});

router.get('/verify', isLoggedIn, function (req, res) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    res.send(req.user);
});

// TODO: Move isLoggedIn to middleware helper
// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }

    // if they aren't redirect them to the home page
    res.status(401).end();
    // res.redirect('/');
}

module.exports = router;
