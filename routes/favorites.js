var express = require('express');
var router = express.Router();
var User = require('../models/user');
var igdb = require('../igdb-wrapper');
igdb.set_key("PsXs3W1kEwmshcWJ6GvKvOzcYR18p15jSKXjsnsz1eU17oFFZB");

var userId = '57fa8e0ab9d9601c583066fb';


router.get('/', isLoggedIn, function (req, res) {
    var userId = req.user.id;
    User.findById(userId,
        (err, user) => {
            res.send(user.local.favorites);
        });
});

router.get('/extended', isLoggedIn, function (req, res) {
    var userId = req.user.id;
    User.findById(userId,
        (err, user) => {
            if (user.local.favorites.length == 0) return res.send();
            let q = req.url.replace('extended', 'games/' + user.local.favorites.join(','));
            return igdb.get(q, json => res.send(json.body));
        });
});

router.post('/', isLoggedIn, function (req, res) {
    var userId = req.user.id;
    let gameId = req.body.id
    User.findByIdAndUpdate(userId, {$push: {'local.favorites': gameId}},
        {safe: true, upsert: true, new: true},
        (err, user) => {
            if (err) res.status(405).send(err);
            res.end();
        });
});

router.delete('/:id', isLoggedIn, function(req, res) {
    var userId = req.user.id;
    var gameId = req.params.id;
    User.findByIdAndUpdate(userId, {$pull: {'local.favorites': gameId}},
        {safe: true, upsert: true, new: true},
        (err, user) => {
            if (err) res.status(405).send(err);
            res.end();
        });
});

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        return next();
    }

    // if they aren't redirect them to the home page
    res.redirect('/');
}

module.exports = router;
