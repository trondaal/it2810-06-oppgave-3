# GameGuru #

![GameGuru bit.png](https://bitbucket.org/repo/9rKbpn/images/520933023-GameGuru%20bit.png)

## About ##
GameGuru is a gaming community website, which allows users to browse and discover old, new and upcoming games. Registered users can add games to a favorite list, which can be viewed and searched.
The game data is being provided by an external API, from igdb.com. This includes data such as game titles, genres, description, release dates, screenshots and other static data.
A local instance of MongoDB is used for storing user data such as login credentials and favorites.

## Architecture
The application uses a basic client-server architecture, split into a front-end and back-end system. The back-end exposes a number of endpoints which the front-end accesses through AJAX requests. The back-end also exposes a proxy api for the IGDB api, decoupling the front-end from the data provider. Authentication is handled internally, but the database is set up so that integrating facebook, google or other login options would be simple. 

### Front-end
The front-end is built with bare ReactJS, using a purely unidirectional data flow. Using an implementation of the Flux architecture such as Redux was considered, but ultimately decided against to keep the project simple. Project dependencies are managed by Javascript Packer Manager (JSPM), which utilizes the SystemJS loader, offering ES2015 dynamic loading capabilities. BabelJS is used for JSX support and ES2015 transpiling.
Some other noteworthy modules:

* ChartJS - Pretty charts and graphs
* React Router - Client-side URL routing
* React Bootstrap - CSS styles

### Back-end
The back-end runs on NodeJS and the Express framework, and dependencies are handled by NPM. The server exposes a number of endpoints, of which some require a valid session state. Some notable modules:

* Express - Basic Node modifications and improvements
* Mongoose - MongoDB library
* Passport - Authentication and authorization
* Unirest - Simple REST request library

#### Database
The application uses a local MongoDB instance to store user data, such as login credentials and favorites.

#### Authentication
Authentication is handled by Node through Passport, which uses sessions and cookies for authentication. The client uses a local session store that is updated on each DOM render to determine if the user is authenticated, but all requests that require authentication are also checked server-side before being processed. The User model is set up so that implementing 3rd party login options would be easy.

## Project Structure
The project is split into a server and client part. The root of the project contains the server files, while all client files are in the /public sub-folder.

`/package.json` Package configuration for JSPM and NPM

### Back-end `/`
* `/bin/` Node startup script
* `/config/` Configuration files
* `/igdb-wrapper/` Node module for accessing the IGDB api
* `/models/` Mongoose models
* `/routes/` Node routes
* `/app.js` Node setup

### Front-end `/public`
* `/img/` Image files
* `/src/` Page components
* `/src/components` Sub-components
* `/src/config/` Configuration files
* `/src/bootstrap.js` React bootstrap
* `/stylesheets/` CSS
* `/index.html` Index page
* `/jspm.config.js` JSPM configuration

## Project Requirements ##
* Data: The application utilizes a large database made available through an external REST api.
* Hosting: The webserver is a NodeJS server, hosted on a student VM.
* Frontend: The client is built with ReactJS.
* Local database: The application uses a local MongoDB instance to store user data such as login credentials and favorites. Favorites are searchable.
* Search: The application provides search capabilities for games and favorites.
* Sorting: Games can be sorted by name or release date.
* Filtering: Games can be filtered by genre and category.
* Dynamic results: Game search provides asynchronous loading of the result.
* User page: "My favorites" provides user page functionality.
* Sessions: Sessions are utilized for authentication.
* Graphical data display: The stats page displays some interesting data using bar charts.
 
## Possible Improvements
### Caching
There is noticeable delay when requesting data from the IGDB api. The data for recent or commonly used requests such as popular, recent and upcoming games could be cached and served directly from the server. This could be implemented without affecting the front-end and would improve response times significantly.

### External authentication
The application was built with external authentication in mind, but this was left out due to time restrictions. The Mongoose User model has support for Facebook and Google authentication.

### Reviews
The application was initially supposed to support user reviews, but this was also left out due to time restrictions. Adding support for reviews can be done by extending the local MongoDB instance with a reviews document with references to the owning user and the id of the game being reviewed.

## Choice of front-end framework
The decision of choosing between Angular2 or ReactJS was not an easy one. Both offer their own set of advantages and disadvantages, and a lot of it comes down to preference. ReactJS is a relatively light front-end library, allowing it to integrate easily, and is well known for it’s great performance, largely due to it’s use of a virtual DOM. It has been gaining a lot of popularity recently, and it doesn’t look like that’s gonna stop anytime soon. It can be combined with Flux/Redux for a more complete architecture solution. 

Angular2 is a complete front-end framework, offering more in terms of features and API than React/Flux in combination. It is fairly opinionated, which can be both an advantage and a disadvantage depending on it’s use and the project.Angular2 is fairly new, and is a major step forward compared to Angular, both in performance and API.

There is also a big difference in how views are defined. ReactJS allows HTML in Javascript using JSX, while Angular2 places Javascript into HTML templates using special ng-prefixed attributes.
Both ReactJS and Angular2 would be a good fit for our project, which is a simple data-driven application with authentication.

In the end, we decided to go with ReactJS, mostly due to preference. ReactJS is quick to learn, and hugely popular in the web development industry, so gaining some experience with ReactJS is likely a good idea. ReactJS also exposes more of the decision making and implementation to the developers, making it a better tool for learning web development in general as opposed to learning a specific framework. 

## Testing
Due to time restrictions we did not manage to rig a unit testing framework for our application. We did however look into several possible solutions in an attempt to find something that would work well with our system. It was found that some of the well known testing frameworks, such as Karma, do not work all that well with JSPM. This is partly due to overlapping functionality, and using Karma would require several workarounds that would make the testing process unnecessarily complicated, even with the karma-jspm library. One viable option would be Mocha, which seems to be very lightweight and easily integrates with the SystemJS loader provided by JSPM, without any modifications. Another option is systemjs-jest, which acts as a stopgap between JEST, which is normally used to mock CommonJS modules, and SystemJS provided by JSPM. We also believe that the workflow of using Karma with JSPM will improve as JSPM and SystemJS is becoming more popular, especially since SystemJS looks to become the standard way of loading libraries in native JS.

## Setup

### For development
In the project directory: install dependencies, bundle the js files and start the server

```
#!terminal

npm install
jspm install
jspm bundle src -wid
node bin/www
```
Open a web browser and navigate to [localhost:3000](Link URL)

### For production
In the project directory: install dependencies, bundle the js files and start the server

```
#!terminal

npm install
jspm install
cd public/
jspm build src/bootstrap.js --minify
cd ../
sudo MONGODB_URI=mongodb://localhost/gg PORT=80 forever start bin/www
```

Open a web browser and navigate to the URL of the site.