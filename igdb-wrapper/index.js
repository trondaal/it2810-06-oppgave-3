/**
 * Created by larsk on 07-Oct-16.
 */

var key;
var unirest = require('unirest');
var qs = require('querystring');

exports.printMsg = function() {
    console.log("This is a message from the demo package");
}

exports.set_key = function(newKey) {
    key = newKey;
}

/*
Returns the path of an image
Size = cover_{small, med, big}, screenshots_{small, med, big}, thumb{ " ", _widescreen_big}
*/
exports.get_img_path = function(size, hash) {
  return "https://res.cloudinary.com/igdb/image/upload/t_" + size + "_2x/" + hash + ".jpg"
}

/*
Returns a list of cloudinary_id
Image_type = cover, screenshots
*/
exports.get_cloudinary_id = function(id, image_type, callback) {
  unirest.get("https://igdbcom-internet-game-database-v1.p.mashape.com/games/" + id)
      .query('fields=' + image_type + '.cloudinary_id')
      .header("X-Mashape-Key", key)
      .header("Accept", "application/json")
      .end(function (result) {
          callback(result);
      });
}

/*
Returns a list of video_id
this is the id of a youtube video,
www.youtube.com/watch?v=<video_id>
*/
exports.get_video_id = function(id, callback) {
  unirest.get("https://igdbcom-internet-game-database-v1.p.mashape.com/games/" + id)
      .query('fields=videos.video_id')
      .header("X-Mashape-Key", key)
      .header("Accept", "application/json")
      .end(function (result) {
          callback(result);
      });
}

/*
A slug is a string containing the name of the game, hyphonated and in lower-case
eg. "the-legend-of-zelda-breath-of-the-wild"
*/
exports.get_id_by_slug = function(resource, slug, callback) {
  unirest.get("https://igdbcom-internet-game-database-v1.p.mashape.com/"+ resource +"/")
      .query('filter[slug][eq]=' + slug)
      .header("X-Mashape-Key", key)
      .header("Accept", "application/json")
      .end(function (result) {
          callback(result);
      });
}

exports.get_popular_games = function(limit, date, callback) {
  unirest.get("https://igdbcom-internet-game-database-v1.p.mashape.com/games/")
      .query('fields=name')
      .query('limit=' + limit)
      .query('order=popularity:desc')
      .query('filter[release_dates.date][gte]=' + date)
      .header("X-Mashape-Key", key)
      .header("Accept", "application/json")
      .end(function (result) {
          callback(result);
      });
}

exports.search = function(resource, searchString, callback) {
  unirest.get("https://igdbcom-internet-game-database-v1.p.mashape.com/" + resource +"/")
      .query('fields=name')
      .query('order=popularity:desc')
      .query('search=' + searchString)
      .header("X-Mashape-Key", key)
      .header("Accept", "application/json")
      .end(function (result) {
          callback(result);
      });
}

exports.get = function(resource, callback) {
  unirest.get("https://igdbcom-internet-game-database-v1.p.mashape.com/" + resource)
      .header("X-Mashape-Key", key)
      .header("Accept", "application/json")
      .end(function (result) {
          callback(result);
      });
}
